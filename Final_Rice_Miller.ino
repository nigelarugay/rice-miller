#include <LiquidCrystal_I2C.h>
#include <Keypad.h>
#include <Servo.h>
#include "HX711.h"

#define LOADCELL_DOUT_PIN  13
#define LOADCELL_SCK_PIN  12

HX711 scale;
Servo servo;
int servoPosition = 0;

const byte ROWS = 4; 
const byte COLS = 3; 

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte rowPins[ROWS] = {9, 8, 7, 6};
byte colPins[COLS] = {5, 4, 3};
int piezoPin = 11;
int MOTOR = 22;

LiquidCrystal_I2C lcd(0x27,20,4);
Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

String input = "";
int motorSpeed = 0;
int riceWeight = 0;
float startedTime = 0;
float finishedTime = 0;
float averageScale = 0;
boolean isSend = false;

void setup() {
  Serial.begin(9600);
  Serial.println("Starting...");

  pinMode(piezoPin, OUTPUT);
  pinMode(MOTOR, OUTPUT);
  digitalWrite(MOTOR, LOW);
  servo.attach(10);

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale.read();
  scale.set_scale(200.f);
  scale.tare(); 
  
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Starting...");
  servo.write(0);
  
  displayInput();
}

void loop() {
  char customKey = customKeypad.getKey();
  averageScale = scale.get_units();
  Serial.println(averageScale);
  
  if(riceWeight != 0 && riceWeight <= averageScale && !isSend){
    tone(piezoPin, 1300, 3000);
    finishedTime = (millis() - startedTime) / 1000;
    isSend = true;
    Serial.println("speed:" + String(motorSpeed));
    Serial.println("weight:" + String(riceWeight));
    Serial.println("millis:" + String(finishedTime));
    // ioff na yung fucking driller
    digitalWrite(MOTOR, LOW);
  }
  
  if (customKey){
    tone(piezoPin, 1300, 200);

    if(customKey != '#'){
      input += customKey;
      Serial.println("Input: " + input);
      
      if(motorSpeed == 0){
        lcd.setCursor(12,0);
        lcd.print(input); 
      }
      else if(riceWeight == 0){
        lcd.setCursor(11,1);
        lcd.print(input); 
      }
    }

    if(customKey == '#'){
      if(motorSpeed == 0){
        motorSpeed = input.toInt();
        Serial.println("Motor Speed: " + String(motorSpeed));
        input = "";
      }
      else if(riceWeight == 0){
        riceWeight = input.toInt();
        Serial.println("Desired Rice Weight: " + String(riceWeight));
        input = "";
      }

      if(riceWeight != 0 && motorSpeed != 0){
        startedTime = millis();
        // adjust muna yung servo
        if(motorSpeed == 1){
          Serial.println("Level 1");
          servo.write(60); 
        }
        else if(motorSpeed == 2){
          Serial.println("Level 2");
          servo.write(120);
        }
        else if(motorSpeed == 3){
          Serial.println("Level 3");
          servo.write(180);
        }
        // start yung miller;
        digitalWrite(MOTOR, HIGH);
        Serial.println("Motor Starting...");
      }
    }

    if(customKey == '*'){ // Reset Keypad
      displayInput();
      reset();
    }
  }
  
}

void displayInput(){
  tone(piezoPin, 1250, 1000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Motor Speed:");
  lcd.setCursor(0,1);
  lcd.print("Weight (g): ");
}

void reset(){
  tone(piezoPin, 50, 200);
  input = "";
  motorSpeed = 0;
  riceWeight = 0;
  servo.write(0);
  digitalWrite(MOTOR, LOW);
  isSend = false;
}
